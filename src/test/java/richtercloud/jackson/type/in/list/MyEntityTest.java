package richtercloud.jackson.type.in.list;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Arrays;
import java.util.LinkedList;
import static org.junit.Assert.*;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author richter
 */
public class MyEntityTest {
    private final static Logger LOGGER = LoggerFactory.getLogger(MyEntityTest.class);

    /**
     * Test of getSomeProperty method, of class MyEntity.
     */
    @Test
    public void testJsonSerialization() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        long myEntityId = 1l;
        //test single
        MyEntity myEntity = new MyEntity(0,
                myEntityId,
                "");
        String expResultSingle = String.format("{\n" +
                "  \"type\" : \"richtercloud.jackson.type.in.list.MyEntity\",\n" +
                "  \"id\" : %d,\n" +
                "  \"myProperty\" : \"\",\n" +
                "  \"someProperty\" : 0\n" +
                "}",
                myEntityId);
        String result = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(myEntity);
        assertEquals(expResultSingle,
                result);
        //test list
        String expResultList = String.format("[ %s ]",
                expResultSingle);
        result = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(new LinkedList<>(Arrays.asList(myEntity)));
        assertEquals(expResultList,
                result);
    }
}
