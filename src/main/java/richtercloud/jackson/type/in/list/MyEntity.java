package richtercloud.jackson.type.in.list;

/**
 *
 * @author richter
 */
public class MyEntity extends MySuperEntity {
    private int someProperty;

    public MyEntity() {
    }

    public MyEntity(int someProperty, Long id, String myProperty) {
        super(id, myProperty);
        this.someProperty = someProperty;
    }

    public int getSomeProperty() {
        return someProperty;
    }

    public void setSomeProperty(int someProperty) {
        this.someProperty = someProperty;
    }
}
